package com.homework;

/**
 * Created by Valeriy on 07.10.2016.
 */
 public class Student extends Man{
    private int educationYear;

    public void setEducationYear(int educationYear) {
        this.educationYear = educationYear;
    }

    public void upEducationYear(int upEducationYear) {
        this.educationYear += educationYear;
    }
}