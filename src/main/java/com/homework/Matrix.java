package com.homework;

import java.util.Random;

/**
 * Created by Valeriy on 04.10.2016.
 */
public class Matrix {

    private int height,width;
    private int [][] matrix;


    public Matrix(int height, int width){
        this.matrix = new int[height][width];
        this.width=width;
        this.height=height;
        Random rnd = new Random();
        for(int i=0;i<height;i++){
            for (int j=0;j<width;j++){
                this.matrix[i][j]=rnd.nextInt()%50;
            }
        }
    }

    public Matrix(int[][] matrix){
        this.matrix=matrix;
        this.height=matrix.length;
        this.width=matrix[0].length;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
        this.height=matrix.length;
        this.width=matrix[0].length;
    }

    public void addMatrix(int matrix2[][]){
        if(matrix2.length!=matrix.length||matrix2[0].length!=matrix[0].length){
            System.out.println("Разные размеры матриц"); return;
        }
        System.out.println("arr+mtrx=");
        for (int i=0;i<matrix2.length&&i<matrix.length;i++){
            for(int j=0;j<matrix2[0].length&&j<matrix[0].length;j++){
                matrix[i][j] +=matrix2[i][j];
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println("");
        }
    }

    public void mulNumMatrix(int number){
        System.out.println("mtrx*"+number+"=");
        for (int i=0;i<matrix.length&&i<matrix.length;i++){
            for(int j=0;j<matrix[0].length&&j<matrix[0].length;j++){
                matrix[i][j] *=number;
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println("");
        }
    }

    public void mulMatrixMatrix(int matrix2[][]){
        System.out.println("mtrx*arr2=");
        if (matrix[0].length!=matrix2.length){
            System.out.println("Матрицы умножить нельзя"); return;
        }

        int result[][] = new int[matrix.length][matrix2[0].length];
        for (int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix2[0].length;j++){
                for (int k=0;k<matrix2.length;k++) {
                        result[i][j] += matrix[i][k]*matrix2[k][j];
                }
                System.out.print(result[i][j] + " ");
            }
            System.out.println("");
        }
        matrix=result;
    }

    public void transposition(){
        System.out.println("matrix(transp)=");
        int result[][] = new int[matrix[0].length][matrix.length];
        for(int i=0;i<result.length;i++){
            for(int j=0;j<result[0].length;j++){
                result[i][j]=matrix[j][i];
                System.out.print(result[i][j]+" ");
            }
            System.out.println("");
        }
        matrix=result;
    }

    @Override
    public String toString(){
        String string=null;
        for(int i=0;i<matrix.length;i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if(string==null)
                    string=String.valueOf(matrix[0][0])+" ";
                else
                    string += (String.valueOf(matrix[i][j]) + " ");
            }
            string+="\n";
        }
        return string;
    }

    public static void main(String[] args) {

        int arr[][] = new int[5][4];
        Random rnd = new Random();
        System.out.println("arr:");
        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr[0].length;j++){
                arr[i][j]=rnd.nextInt()%50;
                System.out.print(arr[i][j]+" ");
            }
            System.out.println("");
        }
        System.out.println("");
        Matrix mtrx = new Matrix(5,4);
        System.out.println("mtrx:");
        for(int i=0;i<mtrx.matrix.length;i++){
            for(int j=0;j<mtrx.matrix[0].length;j++){
                System.out.print(mtrx.matrix[i][j]+" ");
            }
            System.out.println("");
        }
        System.out.println("");
        mtrx.addMatrix(arr);
        System.out.println("");
        mtrx.mulNumMatrix(5);
        System.out.println("");
        int arr2[][] = new int[4][6];
        System.out.println("arr2:");
        for(int i=0;i<arr2.length;i++){
            for(int j=0;j<arr2[0].length;j++){
                arr2[i][j]=rnd.nextInt()%50;
                System.out.print(arr2[i][j]+" ");
            }
            System.out.println("");
        }
        System.out.println("");
        mtrx.mulMatrixMatrix(arr2);
        System.out.println("");
        mtrx.transposition();
        System.out.println("");
        System.out.println(mtrx.toString());
    }
}
